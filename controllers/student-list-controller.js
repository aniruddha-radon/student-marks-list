app.controller("studentController", function ($scope) {

    // initializing a blank array to store all students.
    $scope.studentList = [];

    // student will hold all the information of the 'current' student.
    // current means, the student information being filled up right now.
    // this object will be pushed into the studentList array.
    $scope.student = {
        name: '',
        roll_no: '',
        marks: {
            subject1: '',
            subject2: '',
            subject3: ''
        },
        percentage: '',
        //setting showPercentage to false as default. (We do not want to show percentage immediately.)
        showPercentage: false
    };


    // the resetStudent function is going to reset all the values of all keys after
    // the student has been added. This is going to blank out all the fields from the form.
    $scope.resetStudent = function() {
        $scope.student = {
            name: '',
            roll_no: '',
            marks: {
                subject1: '',
                subject2: '',
                subject3: ''
            },
            percentage: '',
            showPercentage: false
        };
    }

    // the addStudent function is goint to add a student to the studentList array.
    // it also, calculates the percentage and stores it in the percentage key of the student object.
    $scope.addStudent = function() {
        var { subject1, subject2, subject3 } = $scope.student.marks; // this pattern is called as object destructuring.
        /** 
         * you can also do -
         * var subject1 = $scope.student.marks.subject1;
         * var subject2 = $scope.student.marks.subject2;
         * var subject3 = $scope.student.marks.subject3;
         */

        // calculating percentage.
        $scope.student.percentage = Math.round((parseInt(subject1) + parseInt(subject2) + parseInt(subject3)) / 3);

        //adding the object to studentList.
        $scope.studentList.push($scope.student);

        // resetting the student object by calling the resetStudent method defined above.
        $scope.resetStudent();
    }


    // function to set the showPercentage to true. 
    // function accepts an index (the position in the array) and sets only the showPercentage at that index to true
    $scope.showPercentage = function(index) {
        $scope.studentList[index].showPercentage = true;
        // if index is 0, '$scope.studentList[index]' is going to fetch the first object (the object at index 0)
        // that object is similar to the $scope.student object.
        // it has the property showPercentage set to false by default.
        // with ' $scope.studentList[index].showPercentage' we are accessing the property
        // with ' $scope.studentList[index].showPercentage = true' we are setting the value of the property to true.
    }
})